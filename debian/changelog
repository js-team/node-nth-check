node-nth-check (2.1.1-4) unstable; urgency=medium

  * Team upload.
  * Add patch for typescript 5.0 compatibility (Closes: #1090016)
  * Bump standards version to 4.7.0 (no changes needed)

 -- Ananthu C V <weepingclown@disroot.org>  Fri, 20 Dec 2024 01:46:17 +0530

node-nth-check (2.1.1-3) unstable; urgency=medium

  * Team upload
  * Back to unstable

 -- Yadd <yadd@debian.org>  Sun, 16 Jul 2023 08:41:49 +0400

node-nth-check (2.1.1-2) experimental; urgency=medium

  * Team upload
  * Declare compliance with policy 4.6.2
  * Build missing ES files

 -- Yadd <yadd@debian.org>  Sun, 21 May 2023 07:23:05 +0400

node-nth-check (2.1.1-1) experimental; urgency=medium

  * Team upload
  * Fix uscan script
  * New upstream release: add sequence and generate method.

 -- Bastien Roucariès <rouca@debian.org>  Sat, 20 May 2023 21:08:16 +0000

node-nth-check (2.0.1-2) unstable; urgency=medium

  * Team upload
  * Drop build dependency to node-typescript-types (Closes: 979785)

 -- Yadd <yadd@debian.org>  Sun, 01 May 2022 13:49:41 +0200

node-nth-check (2.0.1-1) unstable; urgency=medium

  * Team upload
  * Fix GitHub tags regex
  * Update standards version to 4.6.0, no changes needed.
  * Fix filenamemangle
  * New upstream version 2.0.1 (Closes: CVE-2021-3803)

 -- Yadd <yadd@debian.org>  Sat, 05 Feb 2022 08:20:51 +0100

node-nth-check (2.0.0-1) unstable; urgency=medium

  * Team upload
  * Declare compliance with policy 4.5.1
  * New upstream version 2.0.0
  * Build with typescript
  * Update test
  * Add a "Breaks" against node-css-select < 2.1.0+dfsg-3~

 -- Xavier Guimard <yadd@debian.org>  Wed, 16 Dec 2020 11:34:58 +0100

node-nth-check (1.0.2-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

  [ Xavier Guimard ]
  * Bump debhelper compatibility level to 13
  * Declare compliance with policy 4.5.0
  * Add "Rules-Requires-Root: no"
  * Change section to javascript
  * Add debian/gbp.conf
  * debian/watch: use GitHub tags
  * New upstream version 1.0.2
  * Update copyright
  * Use dh-sequence-nodejs auto install
  * Enable upstream test

 -- Xavier Guimard <yadd@debian.org>  Thu, 29 Oct 2020 20:15:05 +0100

node-nth-check (1.0.1-1) unstable; urgency=medium

  * Initial release

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 05 Mar 2016 12:14:44 +0100
